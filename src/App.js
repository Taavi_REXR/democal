import React from 'react';
import Calendar from './components/Calendar';
import logo from './img/logo.png';
import './App.css';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header>
          <div id="navarea">
            <img className="logoicon" src={logo} alt={""} />
            <span className="calendaricon">date_range</span>
            <span>
              react<b>calendar</b>
            </span>
          </div>
        </header>
        <main>
          <Calendar />
        </main>
      </div>
    );
  }
}

export default App;
