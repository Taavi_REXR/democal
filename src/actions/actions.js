const FETCH_EVENTS_REQUEST = 'FETCH_EVENTS_REQUEST'
const FETCH_EVENTS_SUCCESS = 'FETCH_EVENTS_SUCCESS'
const FETCH_EVENTS_FAILURE = 'FETCH_EVENTS_FAILURE'

export const fetchEventsRequest = () => {
  return {
    type: FETCH_EVENTS_REQUEST
  }
}

export const fetchEventsSuccess = (eventsData, startDate) => {
  return {
    type: FETCH_EVENTS_SUCCESS,
    payloadEvents: eventsData,
    payloadStartDate: startDate
  }
}

export const fetchEventsFailure = () => {
  return {
    type: FETCH_EVENTS_FAILURE,
  }
}
