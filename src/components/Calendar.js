import React, {useState} from 'react';
import { useDispatch } from "react-redux";
import { store } from '../index';
import { format, getDay, getWeek, addWeeks, subWeeks, startOfWeek, endOfWeek, addDays, subDays} from 'date-fns';
import { fetchEventsRequest, fetchEventsSuccess, fetchEventsFailure } from '../actions/actions';

function Calendar() {
  const dispatch = useDispatch();
  const [currentState, setCurrentState] = useState({}); // This is cutting a corner and enables forcing a rerender
  const [currentDate, setCurrentDate] = useState(new Date())
  var [currentStart, setCurrentStart] = useState(startOfWeek(currentDate))
  const stateformat = "yyyy-MM-dd"
  const startDate = format(startOfWeek(currentDate), stateformat);
  const endDate = format(endOfWeek(currentDate), stateformat);
  const apiKey = process.env.REACT_APP_API_KEY;

  //Fetch only if data not in Redux store already
  //Rather simple API used for this practice app. You can use other APIs as well,
  //but then you need to tailor the code a bit.
  //To use it without Redux, I'd recommend to go with useEffect(() => { }, [e.g startDate]);
  //to make the API call run only once and store the response in state.
  if(!Object.values(store.getState().startDate).includes(startDate)) {
      dispatch(fetchEventsRequest())
      fetch('https://wozmx9dh26.execute-api.eu-west-1.amazonaws.com/api/holidays', {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
        mode: 'cors',
        body: JSON.stringify({
          apiKey: apiKey,
          startDate: startDate,
          endDate: endDate
        })
      })
      .then(res => res.json())
      .then (data => {
          var eventsData = data.holidays;
          dispatch(fetchEventsSuccess(eventsData, startDate));
          setCurrentState({}); // forces a rerender
      })
      .catch(error => {
        console.log(error)
        dispatch(fetchEventsFailure());
      });
  }

  //Calendar control button functions
  const nextWeek = () => {
    setCurrentDate(addWeeks(currentDate, 1));
  }

  const prevWeek = () => {
    setCurrentDate(subWeeks(currentDate, 1));
  }

  const nextStartDay = () => {
    setCurrentStart(addDays(currentStart, 1))
  }

  const prevStartDay = ()=> {
    setCurrentStart(subDays(currentStart, 1))
  }

  //Calendar control panel
  const header = () => {
  const dateFormat = "EEEE";
  return (
     <div className="header">
        <div className="selector">
           <div className="prev" onClick={prevWeek}></div>
           <div className="current">Week {getWeek(currentDate)}</div>
           <div className="next"onClick={nextWeek}></div>
        </div>
        <div className="selector">
          <div className="current">Week starts with:</div>
          <div className="prev" onClick={prevStartDay}></div>
          <div className="current">{format(currentStart, dateFormat)}</div>
          <div className="next" onClick={nextStartDay}></div>
        </div>
     </div>
     );
  };

  // Calendar displays one week at a time.
  const daysOfWeek = () => {
  const dateFormat = "EEEE dd.MM.yyyy";
  const days = [];
  let startDate = startOfWeek(currentDate, {weekStartsOn: getDay(currentStart)});
  var eventnamesarray = [];
  var eventtypesarray = [];

  //Loop through holidays from store and match them with the dates in the selected week.
  //There's likely a better way. This here creates two arrays to store holiday name and type public/folk.
  for (let i = 0; i < 7; i++) {
    for (let j = 0; j < Object.keys(store.getState().events).length; j++) {
      if ( format(addDays(startDate, i), stateformat) === Object.keys(store.getState().events)[j] ) {
        eventnamesarray[i] = Object.values(store.getState().events)[j][0].name;
        eventtypesarray[i] = Object.values(store.getState().events)[j][0].type;
      } else {
        eventnamesarray[j+i] = " ";
        eventtypesarray[j+i] = " ";
      }
    }
     if (eventtypesarray[i] === "public") {
        days.push(
           <div className="daybox" key={i}>
           {format(addDays(startDate, i), dateFormat)}
           <div style={{color: "red"}} key={i}>{eventnamesarray[i]}</div>
           </div>
        );
      } else {
        days.push(
           <div className="daybox" key={i}>
           {format(addDays(startDate, i), dateFormat)}
           <div style={{color: "blue"}} key={i}>{eventnamesarray[i]}</div>
           </div>
        );
      }
     }
     return <div className="weekgrid">{days}</div>;
  };

    return (
      <div className="calendar">
      <div>{header()}</div>
      <div>{daysOfWeek()}</div>
   </div>
    );
};

export default Calendar;
