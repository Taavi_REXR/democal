const FETCH_EVENTS_REQUEST = 'FETCH_EVENTS_REQUEST'
const FETCH_EVENTS_SUCCESS = 'FETCH_EVENTS_SUCCESS'
const FETCH_EVENTS_FAILURE = 'FETCH_EVENTS_FAILURE'

export const reducer = ( state = {events: {}, startDate: []}, action ) => {
  switch(action.type) {
    case FETCH_EVENTS_REQUEST:
      return state;
    case FETCH_EVENTS_SUCCESS:
      return {
        ...state,
        events: Object.assign(state.events, action.payloadEvents),
        startDate: [...state.startDate, action.payloadStartDate]
      };
    case FETCH_EVENTS_FAILURE:
      return {
        events: {},
      };
    default:
      return state;
  }
}
